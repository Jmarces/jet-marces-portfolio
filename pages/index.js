import Image from 'next/image'

import { Row, Button } from 'react-bootstrap'

import { faFacebook, faInstagram, faLinkedin, faGitlab, faHtml5, faJs, faCss3Alt, faNodeJs, faReact } from "@fortawesome/free-brands-svg-icons"
import { faDatabase } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Home() {

	return (

		<>
			<div className="pt-6 md:p-8 text-center md:text-left space-y-4">
				<Image className="w-32 h-32 rounded-full mx-auto" src="/images/Me.jpg" alt="" width="200" height="240" />
				<blockquote>
					<p className="text-lg font-semibold">
						"I'm always up for the challenge ahead, more so with a team or partner on a vision. "
      					</p>
				</blockquote>
				<figcaption className="font-medium">
					<div className="text-4xl text-600">
						Jet Marces
      					</div>
					<div>
						Full-Stack Web Developer
      					</div>
					<div>
						+639477603043 | jmarces16@gmail.com
      					</div>
					<Button variant="dark" target="_blank" href="https://www.facebook.com/jet.marces/" className="my-2  mx-4"><FontAwesomeIcon icon={faFacebook}></FontAwesomeIcon></Button>
					<Button variant="dark" target="_blank" href="https://www.linkedin.com/in/jet-marces-533474163/" className="my-2 mx-4"><FontAwesomeIcon icon={faLinkedin}></FontAwesomeIcon></Button>
					<Button variant="dark" target="_blank" href="https://www.instagram.com/jet_marces/" className="my-2  mx-4"><FontAwesomeIcon icon={faInstagram}></FontAwesomeIcon></Button>
					<Button variant="dark" target="_blank" href="https://gitlab.com/users/Jmarces/projects" className="my-2 mx-4"><FontAwesomeIcon icon={faGitlab}></FontAwesomeIcon></Button>
				</figcaption>

				{/* Skills Section */}

				<div id="Skills">
					<h1 className="text-6xl my-4">Skills</h1>
					<div>
						<h1 className="text-4xl my-4">Front End</h1>
						<div className="inline-block">
							<Image className="rounded" src="/images/Html5.png" alt="HTML5" width="85" height="85" />
							<h1>HTML5</h1>
						</div>
						<div className="inline-block mx-3">
							<Image className="rounded" src="/images/Css3.png" alt="CSS3" width="60" height="85" />
							<h1>CSS3</h1>
						</div>
						<div className="inline-block mx-1">
							<Image className="rounded" src="/images/Tailwind.png" alt="Tailwind" width="95" height="85" />
							<h1>Tailwind</h1>
						</div>
						<div className="inline-block mx-3">
							<Image className="rounded" src="/images/Js.png" alt="Javascript" width="85" height="85" />
							<h1>Javascript</h1>
						</div>
						<div className="inline-block mx-2">
							<Image className="rounded" src="/images/Bootstrap.png" alt="Bootstrap" width="75" height="85" />
							<h1>Bootstrap</h1>
						</div>
						<div className="inline-block">
							<Image className="rounded" src="/images/React.png" alt="React" width="120" height="85" />
							<h1>React</h1>
						</div>
						<div className="inline-block">
							<Image className="rounded" src="/images/Next.png" alt="Next" width="85" height="85" />
							<h1>Next</h1>
						</div>
					</div>
					<div>
						<h1 className="text-4xl my-4">Back End</h1>
						<div className="inline-block mx-3">
							<Image className="rounded" src="/images/Node.png" alt="NodeJS" width="85" height="85" />
							<h1>Node JS</h1>
						</div>
						<div className="inline-block mx-2">
							<Image className="rounded" src="/images/Express.png" alt="ExpressJS" width="85" height="85" />
							<h1>Express JS</h1>
						</div>
						<div className="inline-block mx-2">
							<Image className="rounded" src="/images/Mongo.png" alt="Mongo" width="95" height="85" />
							<h1>Mongo DB</h1>
						</div>
					</div>
				</div>
			</div>

			{/* Projects Section */}

			<div id="Projects">
				<h1 className="text-6xl text-center my-4">My Projects</h1>
				<Row>
					<div className="col-12 col-md-6 text-center mb-5">
						<p className="font-medium">News App with React/Next<FontAwesomeIcon className="text-3xl" icon={faNodeJs}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faJs}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faReact}></FontAwesomeIcon></p>
						<Image className="w-32 h-32 rounded mx-auto" src="/images/News App.png" alt="" width="500" height="250" />
						<br></br>
						<Button variant="info" href="https://news-app-blush-rho.vercel.app/" target="_blank" className="text-center">Visit</Button>
					</div>
					<div className="col-12 col-md-6 text-center mb-5">
						<p className="font-medium">Budget Tracker App <FontAwesomeIcon className="text-3xl" icon={faDatabase}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faNodeJs}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faJs}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faReact}></FontAwesomeIcon></p>
						<Image className="w-32 h-32 rounded mx-auto" src="/images/Budget Tracker App.png" alt="" width="500" height="250" />
						<br></br>
						<Button variant="info" href="https://capstone-3-client-marces.vercel.app/" target="_blank" className="text-center">Visit</Button>
					</div>
					<div className="col-12 text-center mb-5">
						<p className="font-medium">Course Booking App <FontAwesomeIcon className="text-3xl" icon={faDatabase}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faHtml5}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faJs}></FontAwesomeIcon> <FontAwesomeIcon className="text-3xl" icon={faCss3Alt}></FontAwesomeIcon></p>
						<Image className="w-32 h-32 rounded mx-auto" src="/images/Course Booking App.png" alt="" width="500" height="250" />
						<br></br>
						<Button variant="info" href="https://jmarces.gitlab.io/capstone2-marces/" target="_blank" className="text-center">Visit</Button>
					</div>
				</Row>
			</div>
		</>
	)
}