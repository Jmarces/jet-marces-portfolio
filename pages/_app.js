import 'bootstrap/dist/css/bootstrap.min.css'
import 'tailwindcss/tailwind.css'

import { useState, useEffect } from 'react'

import { Container } from 'react-bootstrap'

import NavBar from '../components/NavBar'

import { UserProvider } from '../userContext'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({

		email: null,
		isAdmin: null
	})

	/*
		NextJS is pre-rendered. Your Pages are built initially in the server then passed into browser. localStorage does not exist until the page/component is rendered. We are going to use useEffect to get our email and isAdmin details from our localStorage instead so as to assure that we access the localStorage only after the page/component has initially rendered.
	*/

	useEffect(() => {

		setUser({

			email: localStorage.getItem('email'),
			isAdmin: localStorage.getItem('isAdmin') === "true"
		})
	}, [])

	const unsetUser = () => {

		// clear the localStorage
		localStorage.clear()

		// set the values of our state back to its initial value
		setUser({

			email: null,
			isAdmin: null
		})
	}

	return (

		<>
			<div className="bg-gradient-to-r from-blue-400 via-red-500 to-blue-500 ...">
				<UserProvider value={{ user, setUser, unsetUser }}>
					<NavBar />
					<Container>
						<Component {...pageProps} />
					</Container>
				</UserProvider>
			</div>
		</>
	)
}

export default MyApp