import { useContext } from 'react'

import { Navbar, Nav } from 'react-bootstrap'

import Link from 'next/link'

import UserContext from '../userContext'

export default function NavBar() {

	const { user } = useContext(UserContext)

	// console.log(user)

	return (

		<Navbar expand="lg">

			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto mr-auto">
					<Link href="/">
						<a className="navbar-brand">Jet</a>
					</Link>
					<Link href="#Projects">
						<a className="nav-link" role="button">Projects</a>
					</Link>
					<Link href="#Skills">
						<a className="nav-link" role="button">Skills</a>
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}